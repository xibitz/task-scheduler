package com.task.sch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestCollectActive  implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(RequestCollectActive.class);
    private String message;
    private String source;

    public RequestCollectActive(String source,String message) {
        this.source = source;
        this.message = message;
    }

    @Override
    public void run() {
        logger.info("[{}] messages : {}",source,message);
    }
}
