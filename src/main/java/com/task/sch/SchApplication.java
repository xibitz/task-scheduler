package com.task.sch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@SpringBootApplication
public class SchApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(SchApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SchApplication.class, args);
	}

	@Autowired
	private TaskScheduler taskScheduler;

	private final Map<String, ScheduledFuture<?>> listSch = new HashMap<>();

	@Override
	public void run(String... args) throws Exception {

		final String source = "testId";
		final String cronSample = "*/10 * * * * *";
		final String sampleMessages = "Hello Worlds!!!";

		logger.info("starting sample sch for {} with message {} in {}",source,sampleMessages,cronSample);
		ScheduledFuture<?> scheduledFuture = taskScheduler.schedule(new RequestCollectActive(source,sampleMessages), new CronTrigger(cronSample));
		listSch.put(source,scheduledFuture);
	}
}
